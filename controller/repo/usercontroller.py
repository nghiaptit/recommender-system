import pandas as pd

from common.constants import Constants


class UserController:
    def __init__(self):
        self

    def check_user(self, json_data):
        username = json_data['username']
        password = json_data['password']
        data_user = pd.read_csv(Constants.DATASETS_DIRECTORY + "users.csv", dtype={'password': str})
        content = data_user.loc[(data_user['username'] == username) & (data_user['password'] == password)]
        if (len(content) > 0):
            return content.reset_index().to_json(orient='records')
        return "{}"
