import os

from common.constants import Constants
from flask import send_file

class ImageController:
    def __init__(self):
        self

    def root_dir(self):  # pragma: no cover
        return os.path.abspath(os.path.dirname(__file__))

    def get_image(self, name):
        try:
            src = os.path.dirname(os.getcwd()) + "\\data\\resource\\images\\" + name
            return send_file(src, mimetype='image/gif')
        except IOError as exc:
            return str(exc)
