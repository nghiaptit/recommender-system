import pandas as pd

from common.constants import Constants
from controller.repo.iofile import ReadFile


class Formatter:
    def __init__(self):
        self

    def generation_user_data(self):
        iofile = ReadFile('users_items.csv')
        users_items = iofile.data

        columns = ['userid', 'username', 'password', 'age', 'email', 'address']
        data_frame = pd.DataFrame(columns=columns)
        for i in range(0, len(users_items)):
            userid = users_items.loc[i, 'userid']
            username = 'user' + str(userid)
            password = '123456'
            age = 20
            email = 'email' + str(i + 1) + "@gmail.com"
            address = 'US'
            # set data
            data_frame.loc[i, 'userid'] = userid
            data_frame.loc[i, 'username'] = username
            data_frame.loc[i, 'password'] = password
            data_frame.loc[i, 'age'] = age
            data_frame.loc[i, 'email'] = email
            data_frame.loc[i, 'address'] = address

        data_frame.to_csv(Constants.DATASETS_DIRECTORY + "users.csv", encoding='utf-8', index=False)


formatter = Formatter()
formatter.generation_user_data()
