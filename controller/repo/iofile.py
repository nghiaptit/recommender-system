import os.path

import pandas as pd


class ReadFile:
    def __init__(self):
        self

    def get_id_movie(self):
        self.path = os.path.dirname(os.path.dirname(os.getcwd())) + "\\data\\datasets\\rating-official.csv"
        self.data = pd.read_csv(self.path)
        moviesId = self.data.iloc[0:, 1]
        return moviesId;
