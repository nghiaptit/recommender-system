import pandas as pd

from common.constants import Constants


class MovieController:
    def __init__(self):
        self

    def get_movie_by_id(self, movieId):
        movies = pd.read_csv(Constants.DATASETS_DIRECTORY + "movies.csv")
        movie = movies.loc[movies['movieid'] == str(movieId)]
        if (len(movie) > 0):
            return movie.reset_index().to_json(orient='records')
        return "[]"

    def get_movie_random(self):
        movies = pd.read_csv(Constants.DATASETS_DIRECTORY + "movies.csv")
        movielist = movies.sample(n=20, replace=True)

        if (len(movielist) > 0):
            return movielist.reset_index().to_json(orient='records')
        return "{}"

    def get_movie_newest(self):
        movies = pd.read_csv(Constants.DATASETS_DIRECTORY + "movies.csv")
        movielist_sorted = movies.sort_values(by='datePublished', ascending=False)
        movielist = movielist_sorted.iloc[0:20]
        if (len(movielist) > 0):
            return movielist.reset_index().to_json(orient='records')
        return "[]"

    def get_movie_keyword(self, keyword):
        movies = pd.read_csv(Constants.DATASETS_DIRECTORY + "movies.csv")
        listmovie = movies[movies['name'].islower() == keyword.islower()]
        print(listmovie)

