from builtins import print
from operator import itemgetter

import pandas as pd
import numpy as np

from common.constants import Constants
from scipy.spatial.distance import cosine


class DataProcessor:
    def __init__(self, time, location, companion):
        self.time = time
        self.location = location
        self.companion = companion

    def formatDataUserItem(self):
        directory_dataset = Constants.DATASETS_DIRECTORY + "rating-official.csv"
        data = pd.read_csv(directory_dataset)

        data_base = data

        if not self.time == "":
            data_base = data_base.loc[data_base['Time'] == self.time].drop(['Time'], 1)

        if not self.location == "":
            data_base = data_base.loc[data_base['Location'] == self.location].drop(['Location'], 1)

        if not self.companion == "":
            data_base = data_base.loc[data_base['Companion'] == self.companion].drop(['Companion'], 1)

        itemIds = data_base.itemid.unique()
        userIds = data_base.userid.unique()

        data_user_item = pd.DataFrame(index=userIds, columns=itemIds)

        for i in range(0, len(userIds)):
            userid = userIds[i]

            avgRating = self.getAvgRating(data_base, userid)

            for j in range(0, len(itemIds)):
                itemid = itemIds[j]

                data_filtered = data_base.loc[(data_base['userid'] == userid) & (data_base['itemid'] == itemid)]
                if (len(data_filtered) > 0):
                    rating_value = data_filtered.loc[0:, 'rating'].tolist()[0]
                    data_user_item.loc[userid][itemid] = rating_value - avgRating
                    # data_user_item.loc[userid][itemid] = rating_value
                else:
                    data_user_item.loc[userid][itemid] = 0

        data_user_item.to_csv(Constants.DATASETS_DIRECTORY + "users_items_formatted.csv")
        # data_user_item.to_csv(Constants.datasets_directory + "users_items.csv")

    def getAvgRating(self, data, userId):
        dataRatingByUser = data.loc[data['userid'] == userId]
        sum = dataRatingByUser.loc[:, 'rating'].sum()
        return sum / len(dataRatingByUser)

    def calculateSimilarilyOfUser(self):
        # read data
        data_item_base = pd.read_csv(Constants.DATASETS_DIRECTORY + "users_items_formatted.csv")

        # itemsId = data_item_base.columns.delete(0)

        userId = data_item_base.iloc[0:, 0]

        data_item_base_frame = pd.DataFrame(index=userId, columns=userId)
        # data_item_base_frame = pd.read_csv(Constants.datasets_directory + "items_items.csv")

        for i in range(0, len(data_item_base_frame.columns)):
            for j in range(0, len(data_item_base_frame.columns)):
                # data_item_base_frame.iloc[i, j] = 1 - cosine(data_item_base.iloc[:, i], data_item_base.iloc[:, j])
                data_item_base_frame.iloc[i, j] = 1 - cosine(data_item_base.iloc[i, :], data_item_base.iloc[j, :])

        data_item_base_frame.to_csv(Constants.DATASETS_DIRECTORY + 'users_users.csv')

    def calculateSimilarilyOfItem(self):
        # read data
        data_item_base = pd.read_csv(Constants.DATASETS_DIRECTORY + "users_items_formatted.csv")

        itemsId = data_item_base.columns.delete(0)

        # userId = data_item_base.iloc[0:, 0]

        data_item_base_frame = pd.DataFrame(index=itemsId, columns=itemsId)
        # data_item_base_frame = pd.read_csv(Constants.datasets_directory + "items_items.csv")

        for i in range(0, len(data_item_base_frame.columns)):
            for j in range(0, len(data_item_base_frame.columns)):
                data_item_base_frame.iloc[i, j] = 1 - cosine(data_item_base.iloc[:, i], data_item_base.iloc[:, j])
                # data_item_base_frame.iloc[i, j] = 1 - cosine(data_item_base.iloc[i, :], data_item_base.iloc[j, :])

        data_item_base_frame.to_csv(Constants.DATASETS_DIRECTORY + 'items_items.csv')

    def recommend_temp(self, userid):
        items_items = pd.read_csv(Constants.DATASETS_DIRECTORY + "items_items.csv")
        data = pd.read_csv(Constants.DATASETS_DIRECTORY + "users_items.csv")

        index = items_items.columns.delete(0)
        items_items.set_index(index)

        # Initial a frame for save closes neighbors to an item
        data_neighbors = pd.DataFrame(index=items_items.columns, columns=range(1, 11))

        # Order by similarity
        for i in range(0, len(items_items.columns)):
            data_neighbors.iloc[i, :10] = items_items.iloc[0:, i].sort_values(ascending=False)[:10].index

        data_sims = data
        data_sims.iloc[:, :1] = data.iloc[:, :1]

        for i in range(0, len(data_sims.index)):
            for j in range(1, len(data_sims.columns)):
                user = data_sims.index[i]
                item = data_sims.columns[j]

                # 'tt1499658'

                if data.iloc[i][j] == 1:
                    data_sims.iloc[i][j] = 0
                else:
                    product_top_names = data_neighbors.loc[item][1:10]
                    product_top_sims = items_items[item].sort_values(ascending=False)[1:10]
                    user_purchases = items_items.iloc[user, product_top_names]

                    return
                    data_sims.iloc[i][j] = self.similarity_score(user_purchases, product_top_sims)

        data_recommend = pd.DataFrame(index=data_sims.index, columns=['user', '1', '2', '3', '4', '5', '6'])

        data_recommend.ix[0:, 0] = data_sims.ix[:, 0]



    def similarity_score(self, history, similarities):
        return sum(history * similarities) / sum(similarities)

    def recommend_top_ten_by_user(self, userid):
        items_items = pd.read_csv(Constants.DATASETS_DIRECTORY + "items_items.csv")
        data = pd.read_csv(Constants.DATASETS_DIRECTORY + "users_items.csv")
        columns = items_items.index

        result_recommend = pd.DataFrame(columns=items_items.columns.delete(0))

        rating_by_user = data.loc[data['userid'] == userid].iloc[0, 1:]

        for i in range(0, len(columns)):
            item_similarity = items_items.iloc[i, 1:].tolist()
            result = self.calculate_similar(rating_by_user, item_similarity)
            item_name = items_items.columns[i + 1]
            result_recommend.loc[0, item_name] = result

        top10 = result_recommend.iloc[0, 0:].sort_values(ascending=False)[:10]
        return top10

    def calculate_similar(self, rating, itemsimilarity):
        result = 0
        count = 0
        for i in range(0, len(rating)):
            result += rating[i] * itemsimilarity[i]
            if (rating[i] > 0):
                count += 1
        return result / count

    def recommendbyuser(self, userId):
        itemsIdRecommend = self.recommend_top_ten_by_user(userId)
        movies = pd.read_csv(Constants.DATASETS_DIRECTORY + "movies.csv")

        recommend_movies = []
        for i in range(0, len(itemsIdRecommend)):
            movie = movies.loc[movies['movieid'] == itemsIdRecommend.index[i]]
            recommend_movies.append(movie)
            

