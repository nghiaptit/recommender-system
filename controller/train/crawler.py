# importing the requests library
import os
import time
import urllib.request

import pandas as pd

from common.constants import Constants
from controller.crawl.datasaver import DataSaver
from controller.crawl.htmlparser import HTMLSupport
from controller.repo.iofile import ReadFile

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup


class Crawler:
    urlDomain = "https://www.imdb.com/title/"

    def __init__(self):
        self.clone_data()

    def clone_data(self):
        # read file rating movies
        readfile = ReadFile()

        # get movies id from file
        items = readfile.get_id_movie()

        # init path save data sets
        path_movie_dataset = Constants.DATASETS_DIRECTORY + "\\data\\datasets\\"
        path_log = Constants.DATASETS_DIRECTORY + "\\data\\datasets\\log.txt"

        # init log stream to log error
        logger = open(path_log, "w+")
        # logger.write("movieid,context,type,url,name,imageurl,genre,contentrating,actor,"
        #              "director,creator,description,datePublished,keywords,aggregateRating,review,duration,trailer\n")

        # define columns name in movie data set
        columnstring = "movieid,context,type,url,name,imageurl,genre,contentrating,actor," \
                       "director,creator,description,datePublished,keywords,aggregateRating,review,duration,trailer"

        # split column
        columnname = columnstring.split(',')

        # init data frame
        dataframe = pd.DataFrame(columns=columnname)

        # init list movie url requested
        items_request = []

        count_item = 0

        for i in range(0, len(items)):
            count_item += 1
            if not (items[i] in items_request):
                # define url to request
                url = self.urlDomain + items[i]
                try:
                    # response html content
                    html_content = urllib.request.urlopen(url).read()

                    # parse html content to html
                    parsed_html = BeautifulSoup(html_content, "html.parser")

                    # init htmlsupport object to parse html to object
                    htmlsupport = HTMLSupport(parsed_html, items[i])

                    # get movie from html
                    movie = htmlsupport.get_movie()

                    # save image movie
                    # datasaver = DataSaver(movie)
                    # path_image = datasaver.get_path_image()
                    #
                    # if not (path_image == '' or os.path.exists(path_image)):
                    #     datasaver.save_image()
                    # else:
                    #     if path_image == '':
                    #         logger.write(items[i] + " not include image !!")
                    #     else:
                    #         print("----File " + items[i] + ".jpg already exist !!")

                    # save data about movie
                    datamovie = movie.getData()
                    dataarray = datamovie.split('::')

                    dataframe.loc[i] = dataarray

                    if count_item > 50:
                        logger.close()
                        current_time = time.strftime("%Y%m%d-%H%M%S")
                        directory_movie_dataset = path_movie_dataset + "movie_" + current_time + ".csv"
                        dataframe.to_csv(directory_movie_dataset, encoding='utf-8', index=False)

                        # reset
                        count_item = 0
                        logger = open(path_log, "w+")

                    items_request.append(items[i])
                except Exception as e1:
                    print(str(e1))
                    logger.write("--" + "Error: " + url + "\n")


            else:
                print("Item", items[i], "already existed !")
                continue

        logger.close()

        print("Complete !")


crawler = Crawler()
