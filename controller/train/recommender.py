import pandas as pd

from common.constants import Constants


class Recommender:
    def __init__(self):
        self

    def recommend_top_ten_by_user(self, userid):
        items_items = pd.read_csv(Constants.DATASETS_DIRECTORY + "items_items.csv")
        data = pd.read_csv(Constants.DATASETS_DIRECTORY + "users_items.csv")
        columns = items_items.index
        result_recommend = pd.DataFrame(columns=items_items.columns.delete(0))
        rating_by_user = data.loc[data['userid'] == userid].iloc[0, 1:]
        for i in range(0, len(columns)):
            item_similarity = items_items.iloc[i, 1:].tolist()
            result = self.calculate_similar(rating_by_user, item_similarity)
            item_name = items_items.columns[i + 1]
            result_recommend.loc[0, item_name] = result

        top10 = result_recommend.iloc[0, 0:].sort_values(ascending=False)[:10]
        return top10

    def recommendbyuser(self, username):
        try:
            usersData = pd.read_csv(Constants.DATASETS_DIRECTORY + "users.csv")
            user = usersData.loc[usersData['username'] == username]
            userid = int(user['userid'])
            itemsIdRecommend = self.recommend_top_ten_by_user(userid)
            movies = pd.read_csv(Constants.DATASETS_DIRECTORY + "movies.csv")
            recommend_movies = pd.DataFrame(columns=movies.columns)

            for i in range(0, len(itemsIdRecommend)):
                movie = movies.loc[movies['movieid'] == itemsIdRecommend.index[i]]
                recommend_movies = recommend_movies.append(movie)

            if (len(recommend_movies) > 0):
                return recommend_movies.to_json(orient='records')
            return "[]"
        except Exception as e:
            print(e)
            return "[]"

    def similarity_score(self, history, similarities):
        return sum(history * similarities) / sum(similarities)

    def calculate_similar(self, rating, itemsimilarity):
        result = 0
        count = 0
        for i in range(0, len(rating)):
            result += rating[i] * itemsimilarity[i]
            if (rating[i] > 0):
                count += 1
        return result / count
