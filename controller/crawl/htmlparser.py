import json

from model.movie import Movie


class HTMLSupport():
    def __init__(self, html, itemId):
        self.html = html
        self.itemid = itemId

    def get_movie(self):
        data_in_script = self.html.head.find('script', attrs={'type': 'application/ld+json'})
        json_data = json.loads(data_in_script.string)

        movie = Movie()
        movie.id = self.itemid
        movie.context = self.get_value_json(json_data, '@context')
        movie.type = self.get_value_json(json_data, '@type')
        movie.url = self.get_value_json(json_data, 'url')
        movie.name = self.get_value_json(json_data, 'name')
        movie.imageURL = self.get_value_json(json_data, 'image')
        movie.genres = self.get_value_json(json_data, 'genre')
        movie.contentRating = self.get_value_json(json_data, 'contentRating')
        movie.actor = self.get_value_json(json_data, 'actor')
        movie.director = self.get_value_json(json_data, 'director')
        movie.creator = self.get_value_json(json_data, 'creator')
        movie.description = self.get_value_json(json_data, 'description')
        movie.datePublished = self.get_value_json(json_data, 'datePublished')
        movie.keywords = self.get_value_json(json_data, 'keywords')
        movie.aggregateRating = self.get_value_json(json_data, 'aggregateRating')
        movie.review = self.get_value_json(json_data, 'review')
        movie.duration = self.get_value_json(json_data, 'duration')
        movie.trailer = self.get_value_json(json_data, 'trailer')
        return movie

    def get_value_json(self, json, property_name):
        if property_name in json:
            return json[property_name]
        else:
            return ""
