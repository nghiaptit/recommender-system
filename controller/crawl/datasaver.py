import urllib.request as req
import os.path


class DataSaver:
    def __init__(self, movie):
        self.movie = movie
        if self.movie.imageURL != '':
            ext = self.movie.imageURL[-4:]
            self.path = os.path.dirname(os.path.dirname(os.getcwd())) + "\\data\\resource\\images\\" + movie.id + ext
        else:
            self.path = ''

    def save_image(self):
        req.urlretrieve(self.movie.imageURL, self.path)

    def get_path_image(self):
        return self.path
