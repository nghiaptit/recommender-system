import urllib

import pandas as pd
from bs4 import BeautifulSoup

from controller.crawl.htmlparser import HTMLSupport

url = "https://www.imdb.com/title/tt2096673/"

html_content = urllib.request.urlopen(url).read()

# parse html content to html
parsed_html = BeautifulSoup(html_content, "html.parser")

# init htmlsupport object to parse html to object
htmlsupport = HTMLSupport(parsed_html, 'tt2096673')

# get movie from html
movie = htmlsupport.get_movie()

datamovie = movie.getData()
dataarray = datamovie.split('::')

# define columns name in movie data set
columnstring = "movieid,context,type,url,name,imageurl,genre,contentrating,actor," \
               "director,creator,description,datePublished,keywords,aggregateRating,review,duration,trailer"

# split column
columnname = columnstring.split(',')

# init data frame
dataframe = pd.DataFrame(columns=columnname)

dataframe.loc[0] = dataarray

dataframe.to_csv("nghia.csv", encoding='utf-8', index=False)


listmovie = movies[movies['name'].str.lower().contains(keyword.lower())]

print(listmovie['name'])