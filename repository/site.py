from flask import Flask, request

from controller.repo.imagecontroller import ImageController
from controller.repo.moviecontroller import MovieController
from controller.repo.usercontroller import UserController
from controller.train.recommender import Recommender

app = Flask(__name__)
app.env = 'development'
app.config['TESTING'] = True
app.debug = True


@app.route("/")
def hello():
    return "Welcome to Recommender System!"


# User ----------------------------------------------------------------
@app.route('/user/checklogin', methods=['POST'])
def getCheckLogin():
    json_data = request.get_json()
    userController = UserController()
    result = userController.check_user(json_data)
    return result


# END ----------------------------------------------------------------


# Movie --------------------------------------------------------------

@app.route('/movie/<movieId>', methods=['GET'])
def getMovieById(movieId):
    try:
        moiveController = MovieController()
        result = moiveController.get_movie_by_id(movieId)
        return result
    except Exception as e1:
        return e1.__cause__


@app.route('/movie/random', methods=['GET'])
def getMovieRandom():
    try:
        moiveController = MovieController()
        result = moiveController.get_movie_random()
        return result
    except Exception as e1:
        return e1.__cause__


@app.route('/movie/new', methods=['GET'])
def getMovieNew():
    try:
        moiveController = MovieController()
        result = moiveController.get_movie_newest()
        return result
    except Exception as e1:
        return e1.__cause__


@app.route('/movie/search', methods=['POST'])
def getMovieByKeyword():
    try:
        json_data = request.get_json()
        keyword = json_data['keyword']
        moiveController = MovieController()
        result = moiveController.get_movie_keyword(keyword)
        return "Nghia"
    except Exception as e1:
        return e1.__cause__


# END ----------------------------------------------------------------


# Recommend ----------------------------------------------------------

@app.route('/recommend/user/<username>', methods=['GET'])
def getRecommendForUser(username):
    try:
        recommender = Recommender()
        result = recommender.recommendbyuser(username)
        return result
    except Exception as e1:
        return e1.__cause__


# END ----------------------------------------------------------------


# Resource ----------------------------------------------------------

@app.route('/resource/image/<imageName>', methods=['GET'])
def getImageByName(imageName):
    try:
        image = ImageController()
        return image.get_image(imageName)
    except Exception as e1:
        return e1.__cause__


# END ----------------------------------------------------------------


if __name__ == "__main__":
    app.run(host='0.0.0.0')
