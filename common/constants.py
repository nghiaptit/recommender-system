import os


class Constants:
    DATASETS_DIRECTORY = os.path.dirname(os.getcwd()) + "\\data\\datasets\\"
    RESOURCE_DIRECTORY = os.path.dirname(os.path.dirname(os.getcwd())) + "\\data\\resource\\images\\"
    IP_SERVER = "192.168.1.8//"
